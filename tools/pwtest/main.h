/*
 * main.h
 *
 * PWLib application header file for pwtest
 *
 * Copyright 98 Equivalence
 *
 * $Log$
 * Revision 1.15  2001/12/06 04:06:03  robertj
 * Removed "Win32 SSL xxx" build configurations in favour of system
 *   environment variables to select optional libraries.
 *
 * Revision 1.14  2001/11/08 00:31:27  robertj
 * Reinstated URL test code and added "parameter" string dictionary display.
 *
 * Revision 1.13  2001/05/10 02:43:23  robertj
 * Added SSL test code.
 *
 * Revision 1.12  2000/11/12 23:32:36  craigs
 * Added conditionals
 *
 * Revision 1.11  2000/11/09 05:50:23  robertj
 * Added RFC822 aware channel class for doing internet mail.
 *
 * Revision 1.10  2000/08/30 03:17:00  robertj
 * Improved multithreaded reliability of the timers under stress.
 *
 * Revision 1.9  2000/07/15 09:47:35  robertj
 * Added video I/O device classes.
 *
 * Revision 1.8  1999/09/10 04:35:43  robertj
 * Added Windows version of PIPSocket::GetInterfaceTable() function.
 *
 * Revision 1.7  1999/08/07 07:11:14  robertj
 * Added test for menu text change
 *
 * Revision 1.6  1999/07/03 03:12:59  robertj
 * #ifdef'ed test code for stuff not in general distrubution.
 *
 * Revision 1.5  1999/06/24 14:00:09  robertj
 * Added URL parse test
 *
 * Revision 1.4  1999/02/22 10:15:16  robertj
 * Sound driver interface implementation to Linux OSS specification.
 *
 * Revision 1.3  1999/01/31 00:59:27  robertj
 * Added IP Access Control List class to PTLib Components
 *
 * Revision 1.2  1998/12/23 01:15:58  robertj
 * New directory structure
 *
 */

#ifndef _Pwtest_MAIN_H
#define _Pwtest_MAIN_H

#include <pwclib/toolbars.h>


#if ! defined(P_MOTIF) && ! defined(P_LESSTIF) && ! defined(P_QT)
#define	TOPWINDOW_CLASS	PMDIFrameWindow
#else
#define	TOPWINDOW_CLASS	PTopLevelWindow
#endif

class MainMenu;

class MainWindow : public TOPWINDOW_CLASS
{
  PCLASSINFO(MainWindow, TOPWINDOW_CLASS)
  
  public:
    MainWindow(PArgList & args);

    PDECLARE_NOTIFIER(PMenuItem, MainWindow, NewCmd);
    PDECLARE_NOTIFIER(PMenuItem, MainWindow, OpenCmd);
    PDECLARE_NOTIFIER(PMenuItem, MainWindow, CloseCmd);
    PDECLARE_NOTIFIER(PMenuItem, MainWindow, SaveCmd);
    PDECLARE_NOTIFIER(PMenuItem, MainWindow, SaveAsCmd);
    PDECLARE_NOTIFIER(PMenuItem, MainWindow, PrintCmd);
    PDECLARE_NOTIFIER(PMenuItem, MainWindow, PrinterSetupCmd);
    PDECLARE_NOTIFIER(PMenuItem, MainWindow, ExitCmd);

    PDECLARE_COMMAND_ENABLE("Copy", MainWindow, CopyCmd, CanCopy);
    PDECLARE_COMMAND_ENABLE("Copy", MainWindow, PasteCmd, CanPaste);

    PDECLARE_COMMAND("MenuTest", MainWindow, TestCmd);
    PDECLARE_COMMAND("IP_ACL", MainWindow, IP_ACL_Cmd);
    PDECLARE_NOTIFIER(PMenuItem, MainWindow, ListInterfacesCmd);
#if P_SSL
    PDECLARE_COMMAND("TestSSL", MainWindow, TestSSLCmd);
#endif
    PDECLARE_COMMAND("ParseURL", MainWindow, ParseURLCmd);
    PDECLARE_COMMAND("MailTest", MainWindow, MailTestCmd);
    PDECLARE_COMMAND("PlaySound", MainWindow, PlaySoundCmd);
    PDECLARE_COMMAND("RecordSound", MainWindow, RecordSoundCmd);
    PDECLARE_COMMAND("VideoCapture", MainWindow, VideoCaptureCmd);
    PDECLARE_COMMAND("TimerStress", MainWindow, TimerStressCmd);
    PDECLARE_NOTIFIER(PTimer, MainWindow, StressTimeout);

  protected:
    virtual void OnRedraw(PCanvas & canvas);
    virtual void OnResize(const PDim &, ResizeType);
    virtual void OnClose();

  private:
    MainMenu   * myMenu;
    PButtonBar * buttonBar;
    PStatusBar * statusBar;
    PPrintInfo   printInfo;
    PTextFile    TimerStressReport;
};


class Pwtest : public PApplication
{
  PCLASSINFO(Pwtest, PApplication)

  public:
    Pwtest();
    void Main();
};


#endif  // _Pwtest_MAIN_H


// End of File ///////////////////////////////////////////////////////////////
