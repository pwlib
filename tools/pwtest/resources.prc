/*
 * RESOURCES.PRC
 *
 * PWLib application resources file for pwtest
 *
 * Copyright 98 Equivalence
 *
 * $Log$
 * Revision 1.20  2002/12/02 00:15:59  robertj
 * Additional URL testing
 *
 * Revision 1.19  2002/11/19 10:37:13  robertj
 * Added functions to set anf get "file:" URL. as PFilePath and do the right
 *   things with platform dependent directory components.
 *
 * Revision 1.18  2001/12/06 04:06:03  robertj
 * Removed "Win32 SSL xxx" build configurations in favour of system
 *   environment variables to select optional libraries.
 *
 * Revision 1.17  2001/11/08 00:31:27  robertj
 * Reinstated URL test code and added "parameter" string dictionary display.
 *
 * Revision 1.16  2001/05/10 02:43:23  robertj
 * Added SSL test code.
 *
 * Revision 1.15  2000/11/12 23:34:10  craigs
 * Added hierarchical menu test
 *
 * Revision 1.14  2000/11/09 05:50:23  robertj
 * Added RFC822 aware channel class for doing internet mail.
 *
 * Revision 1.13  2000/08/30 03:17:00  robertj
 * Improved multithreaded reliability of the timers under stress.
 *
 * Revision 1.12  2000/07/15 09:47:35  robertj
 * Added video I/O device classes.
 *
 * Revision 1.11  1999/09/10 04:35:43  robertj
 * Added Windows version of PIPSocket::GetInterfaceTable() function.
 *
 * Revision 1.10  1999/08/07 07:11:14  robertj
 * Added test for menu text change
 *
 * Revision 1.9  1999/08/07 01:47:18  robertj
 * Fixed some trivial errors.
 *
 * Revision 1.8  1999/07/03 03:12:59  robertj
 * #ifdef'ed test code for stuff not in general distrubution.
 *
 * Revision 1.7  1999/06/24 14:00:09  robertj
 * Added URL parse test
 *
 * Revision 1.6  1999/02/22 10:15:16  robertj
 * Sound driver interface implementation to Linux OSS specification.
 *
 * Revision 1.5  1999/01/31 08:51:37  robertj
 * Formatting
 *
 * Revision 1.4  1999/01/31 08:43:53  robertj
 * Changed about dialog to be full company name
 *
 * Revision 1.3  1999/01/31 00:59:27  robertj
 * Added IP Access Control List class to PTLib Components
 *
 * Revision 1.2  1998/12/23 01:16:05  robertj
 * New directory structure
 */

#include <pwlib/stdres.h>


#inline hdr
#include "main.h"
#inline end


String @IDS_TITLE              "pwtest";
String @IDS_TEST_BUTTON        "Test!";


MenuBar
{
    Class MainMenu;
    Window MainWindow;

    Menu PSTD_STR_FILE_MENU {
        Item {
            Title        PSTD_STR_NEW_MENU;
            Notify       NewCmd;
            Accelerators PSTD_STR_NEW_ACCEL;
            Id           @IDM_NEW;
        }
        Item {
            Title        PSTD_STR_OPEN_MENU;
            Notify       OpenCmd;
            Accelerators PSTD_STR_OPEN_ACCEL;
            Id           @IDM_OPEN;
        }
        Item {
            Title        PSTD_STR_CLOSE_MENU;
            Notify       CloseCmd;
            Accelerators PSTD_STR_CLOSE_ACCEL;
            Id           @IDM_CLOSE;
        }
        Separator;
        Item {
            Title        PSTD_STR_SAVE_MENU;
            Notify       SaveCmd;
            Accelerators PSTD_STR_SAVE_ACCEL;
            Id           @IDM_SAVE;
        }
        Item {
            Title        PSTD_STR_SAVE_AS_MENU;
            Notify       SaveAsCmd;
            Id           @IDM_SAVE_AS;
        }
        Separator;
        Item {
            Title        PSTD_STR_PRINT_MENU;
            Notify       PrintCmd;
            Accelerators PSTD_STR_PRINT_ACCEL;
            Id           @IDM_PRINT;
        }
        Item {
            Title        PSTD_STR_PRINTER_SETUP_MENU;
            Notify       PrinterSetupCmd;
        }
        Separator;
        Item {
            Title        PSTD_STR_EXIT_MENU;
            Notify       ExitCmd;
            Accelerators PSTD_STR_EXIT_ACCEL;
        }
    }

    Menu PSTD_STR_EDIT_MENU {
        Item {
            Title  PSTD_STR_UNDO_MENU;
            Notify "Undo";
            Accelerators { PSTD_STR_UNDO_ACCEL1,  PSTD_STR_UNDO_ACCEL2  }
        }
        Separator;
        Item {
            Title  PSTD_STR_CUT_MENU;
            Notify "Cut";
            Accelerators { PSTD_STR_CUT_ACCEL1,  PSTD_STR_CUT_ACCEL2  }
        }
        Item {
            Title  PSTD_STR_COPY_MENU;
            Notify "Copy";
            Accelerators { PSTD_STR_COPY_ACCEL1,  PSTD_STR_COPY_ACCEL2  }
        }
        Item {
            Title  PSTD_STR_PASTE_MENU;
            Notify "Paste";
            Accelerators { PSTD_STR_PASTE_ACCEL1, PSTD_STR_PASTE_ACCEL2 }
        }
        Item {
            Title  PSTD_STR_CLEAR_MENU;
            Notify "Clear";
            Accelerators { PSTD_STR_CLEAR_ACCEL1, PSTD_STR_CLEAR_ACCEL2 }
        }
        Separator;
        Item {
            Title        "&Select All";
            Notify       "SelectAll";
            Accelerators "Ctrl+Shift+A";
        }
    }
    Menu "&Test" {
        Item {
            Title  "Menu test";
            Notify "MenuTest";
	    Field  menuTestItem;
        }
        Item {
            Title  "Timer Stress";
            Notify "TimerStress";
        }
        Item {
            Title  "IP Access Control Lists";
            Notify "IP_ACL";
        }
        Item {
            Title  "List Interfaces";
            Notify ListInterfacesCmd;
        }
#ifdef P_SSL1
        Item {
            Title  "SSL Tests";
            Notify "TestSSL";
        }
#endif
        Item {
            Title  "Parse URL";
            Notify "ParseURL";
        }
        Item {
            Title  "Mail Message";
            Notify "MailTest";
        }
        Separator;
        Item {
            Title  "Play Sound";
            Notify "PlaySound";
        }
        Item {
            Title  "Record Sound";
            Notify "RecordSound";
        }
        Item {
            Title  "Video Capture";
            Notify "VideoCapture";
        }
        Separator;
        Item {
            Title  "Timer Stress";
            Notify "TimerStress";
        }
        Separator;
        Menu "Tree" {
        	Item {
            		Title  "Item 1";
        	}
        	Item {
            		Title  "Item 2";
        	}
        	Menu "SubMenu" {
        		Item {
            			Title  "Item 3";
        		}
        		Menu "SubMenu" {
     	   			Item {
            				Title  "Item 4";
        			}
        		}
        	}
        }
    }
}


Icon @IDI_MAIN_WINDOW
{
    Dimensions 32,32,4;
    Colours {
          0,   0,   0,
        128,   0,   0,
          0, 128,   0,
        128, 128,   0,
          0,   0, 128,
        128,   0, 128,
          0, 128, 128,
        128, 128, 128,
        192, 192, 192,
        255,   0,   0,
          0, 255,   0,
        255, 255,   0,
          0,   0, 255,
        255,   0, 255,
          0, 255, 255,
        255, 255, 255
    }
    Pixels {
         0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
         0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
         0  0 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15  0  0 15 15 15 15 15 15 15  0  0 15 15 15  0 15 15 15 15  0  0  0  0  0 15 15 15  0  0
         0  0 15  0  0  0 15 15 15 15 15  0  0 15 15 15 15 15  0  0  0  0  9  9  0 10 10  0  0 15  0  0
         0  0 15 15  0  0  0 15 15 15  0  0 15 15 15 15 15  9 15 15  0  9  9  9  0 10 10 10  0 15  0  0
         0  0 15 15 15  0  0  0 15  0  0 15 15 15 15 15 15 15 15 15  0  9  9  9  0 10 10 10  0 15  0  0
         0  0 15 15 15 15  0  0  0  0 15 15 15 15 15 15 15  9 15 15  0  9  9  9  0 10 10 10  0 15  0  0
         0  0 15 15 15 15 15  0  0  0 15 15 15 15 15 15 15  0 15 15  0  9  0  0  0  0  0 10  0 15  0  0
         0  0 15 15 15 15  0  0  0  0  0 15 15 15 15 15 15 15  0  0  0  0 12 12  0 11 11  0  0 15  0  0
         0  0 15 15 15  0  0 15 15  0  0  0 15 15 15 15 15 12 15 12  0 12 12 12  0 11 11 11  0 15  0  0
         0  0 15 15  0  0 15 15 15 15  0  0  0 15 15 15 15 15 12 15  0 12 12 12  0 11 11 11  0 15  0  0
         0  0 15  0  0 15 15 15 15 15 15  0  0  0 15 15 15 12 15 12  0 12 12 12  0 11 11 11  0 15  0  0
         0  0 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0 12 15  0 12  0  0  0  0  0 11  0 15  0  0
         0  0 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0  0  0 15 15 15 15 15  0  0 15  0  0
         0  0 15 15 15 15 15 15 15  2  2  2 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15 15 15 15 15 15  2  2  2 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15 15 15 15 15 15  2 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15 15 15  2  2  2  2  2  2  2  2 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15 15 11 11 11 11 11 11 11 11 11 11 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15 11 11 11 11 11 11 11 11 11 11 15 15 15 15 15  7  7  7  7  7  7  7  7  7  7 15 15  0  0
         0  0 15 11  9 11  9 11  9 11  9 11 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15  9 11  9 11  9 11  9 11 11 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15  9  9  9  9  9  9  9  9  9 15 15 15 15 15 15  7  7  7  7  7  7  7  7  7  7 15 15  0  0
         0  0 15  9  9  9  9  9  9  9  9  9  9 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15 13 13 13 13 13 13 13 13 13 13 13 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15 15 13 13 13 13 13 13 13 13 13 15 15 15 15 15  7  7  7  7  7  7  7  7  7  7 15 15  0  0
         0  0 15 15 15 12 12 12 12 12 12 12 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15 15 15 15 12 12 12 12 12 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15 15  0  0
         0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
         0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
    }
    AndMask {
        1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
        1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1
        1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
        1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
    }
    XorMask {
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
        0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 0 0 1 1 1 1 1 1 1 0 0 1 1 1 0 1 1 1 1 0 0 0 0 0 1 1 1 0 0
        0 0 1 0 0 0 1 1 1 1 1 0 0 1 1 1 1 1 0 0 0 0 1 1 0 1 1 0 0 1 0 0
        0 0 1 1 0 0 0 1 1 1 0 0 1 1 1 1 1 1 1 1 0 1 1 1 0 1 1 1 0 1 0 0
        0 0 1 1 1 0 0 0 1 0 0 1 1 1 1 1 1 1 1 1 0 1 1 1 0 1 1 1 0 1 0 0
        0 0 1 1 1 1 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 1 1 1 0 1 1 1 0 1 0 0
        0 0 1 1 1 1 1 0 0 0 1 1 1 1 1 1 1 0 1 1 0 1 0 0 0 0 0 1 0 1 0 0
        0 0 1 1 1 1 0 0 0 0 0 1 1 1 1 1 1 1 0 0 0 0 1 1 0 1 1 0 0 1 0 0
        0 0 1 1 1 0 0 1 1 0 0 0 1 1 1 1 1 1 1 1 0 1 1 1 0 1 1 1 0 1 0 0
        0 0 1 1 0 0 1 1 1 1 0 0 0 1 1 1 1 1 1 1 0 1 1 1 0 1 1 1 0 1 0 0
        0 0 1 0 0 1 1 1 1 1 1 0 0 0 1 1 1 1 1 1 0 1 1 1 0 1 1 1 0 1 0 0
        0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 0 1 0 0 0 0 0 1 0 1 0 0
        0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 1 1 1 1 1 0 0 1 0 0
        0 0 1 1 1 1 1 1 1 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 1 1 1 1 1 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 1 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 1 1 0 0
        0 0 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 1 1 0 0
        0 0 1 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 1 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 1 1 0 0
        0 0 1 1 1 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 1 1 1 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    }
}


Image @IDM_TEST_ENABLED
{
    Dimensions 24,22,4;
    Colours {
          0,   0,   0,
        128,   0,   0,
          0, 128,   0,
        128, 128,   0,
          0,   0, 128,
        128,   0, 128,
          0, 128, 128,
        192, 192, 192,
        128, 128, 128,
        255,   0,   0,
          0, 255,   0,
        255, 255,   0,
          0,   0, 255,
        255,   0, 255,
          0, 255, 255,
        255, 255, 255
    }
    Pixels {
         7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7
         7  7  7  7  0  0  0  0  0  0  0  7  7  7  7  7  7  7  7  7  7  7  7  7
         7  7  7  7  0 15 15 15 15  0 15  0  7  7  7  7  7  7  7  7  7  7  7  7
         7  7  7  7  0 15  0  0  0  0  0  0  0  7  7  7  7  7  7  7  7  7  7  7
         7  7  7  7  0 15  0 15 15 15 15  0 15  0  7  7  7  7  7  7  7  7  7  7
         7  7  7  7  0 15  0 15  0  0  0  0  0  0  0  7  7  7  7  7  7  7  7  7
         7  7  7  7  0 15  0 15  0 15 15 15 15  0 15  0  7  7  7  7  7  7  7  7
         7  7  7  7  0 15  0 15  0 15  0  0  0  0  0  0  0  7  7  7  7  7  7  7
         7  7  7  7  0 15  0 15  0 15  0 15 15 15 15  0 15  0  7  7  7  7  7  7
         7  7  7  7  0 15  0 15  0 15  0 15 15 15 15  0 15 15  0  7  7  7  7  7
         7  7  7  7  0 15  0 15  0 15  0 15 15 15 15  0 15 15 15  0  7  7  7  7
         7  7  7  7  0 15  0 15  0 15  0 15 15 15 15  0  0  0  0  0  7  7  7  7
         7  7  7  7  0 15  0 15  0 15  0 15 15 15 15 15 15 15 15  0  7  7  7  7
         7  7  7  7  0  0  0 15  0 15  0 15 15 15 15 15 15 15 15  0  7  7  7  7
         7  7  7  7  7  7  0 15  0 15  0 15 15 15 15 15 15 15 15  0  7  7  7  7
         7  7  7  7  7  7  0  0  0 15  0 15 15 15 15 15 15 15 15  0  7  7  7  7
         7  7  7  7  7  7  7  7  0 15  0 15 15 15 15 15 15 15 15  0  7  7  7  7
         7  7  7  7  7  7  7  7  0  0  0 15 15 15 15 15 15 15 15  0  7  7  7  7
         7  7  7  7  7  7  7  7  7  7  0 15 15 15 15 15 15 15 15  0  7  7  7  7
         7  7  7  7  7  7  7  7  7  7  0  0  0  0  0  0  0  0  0  0  7  7  7  7
         7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7
         7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7
    }
}

//
//  History (disabled)
//

Image @IDM_TEST_DISABLED
{
    Dimensions 24,22,4;
    Colours {
          0,   0,   0,
        128,   0,   0,
          0, 128,   0,
        128, 128,   0,
          0,   0, 128,
        128,   0, 128,
          0, 128, 128,
        192, 192, 192,
        128, 128, 128,
        255,   0,   0,
          0, 255,   0,
        255, 255,   0,
          0,   0, 255,
        255,   0, 255,
          0, 255, 255,
        255, 255, 255
    }
    Pixels {
         7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7
         7  7  7  7  8  8  8  8  8  8  8  7  7  7  7  7  7  7  7  7  7  7  7  7
         7  7  7  7  8  7 15 15 15  8  7  8  7  7  7  7  7  7  7  7  7  7  7  7
         7  7  7  7  8 15  8  8  8  8  8  8  8  7  7  7  7  7  7  7  7  7  7  7
         7  7  7  7  8 15  8  7 15 15 15  8  7  8  7  7  7  7  7  7  7  7  7  7
         7  7  7  7  8 15  8 15  8  8  8  8  8  8  8  7  7  7  7  7  7  7  7  7
         7  7  7  7  8 15  8 15  8  7 15 15 15  8  7  8  7  7  7  7  7  7  7  7
         7  7  7  7  8 15  8 15  8 15  8  8  8  8  8  8  8  7  7  7  7  7  7  7
         7  7  7  7  8 15  8 15  8 15  8  7 15 15 15  8  7  8  7  7  7  7  7  7
         7  7  7  7  8 15  8 15  8 15  8 15  7  7  7  8 15  7  8  7  7  7  7  7
         7  7  7  7  8 15  8 15  8 15  8 15  7  7  7  8 15  7  7  8  7  7  7  7
         7  7  7  7  8 15  8 15  8 15  8 15  7  7  7  8  8  8  8  8  7  7  7  7
         7  7  7  7  8 15  8 15  8 15  8 15  7  7  7  7 15 15 15  8 15  7  7  7
         7  7  7  7  8  8  8 15  8 15  8 15  7  7  7  7  7  7  7  8 15  7  7  7
         7  7  7  7  7 15  8 15  8 15  8 15  7  7  7  7  7  7  7  8 15  7  7  7
         7  7  7  7  7  7  8  8  8 15  8 15  7  7  7  7  7  7  7  8 15  7  7  7
         7  7  7  7  7  7  7 15  8 15  8 15  7  7  7  7  7  7  7  8 15  7  7  7
         7  7  7  7  7  7  7  7  8  8  8 15  7  7  7  7  7  7  7  8 15  7  7  7
         7  7  7  7  7  7  7  7  7 15  8 15  7  7  7  7  7  7  7  8 15  7  7  7
         7  7  7  7  7  7  7  7  7  7  8  8  8  8  8  8  8  8  8  8 15  7  7  7
         7  7  7  7  7  7  7  7  7  7  7 15 15 15 15 15 15 15 15 15 15  7  7  7
         7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7  7
    }
}


ModalDialog {
    Title "About pwtest";
    Dim   165, 135;
    Id    @IDD_ABOUT;
    Class AboutDialog;

    CentreText { Title "pwtest";	      Pos 30, 10;  Dim 131,  8; }
    CentreText { Title "Version 1.0b1";               Pos 30, 20;  Dim 125,  8; }

    StaticIcon { Icon IDI_MAIN_WINDOW;                Pos 10, 10; }

    StaticBox  {                                      Pos  5, 75;  Dim 150, 1; }

    CentreText { Title "This software is free to all personal users.";
                                                      Pos  5, 45;  Dim 150, 10; }
    CentreText { Title "All other users should contact the author for licensing details.";
                                                      Pos 10, 55;  Dim 140, 18; }

    StaticBox  {                                      Pos  5, 40;  Dim 150, 1; }

    CentreText { Title "Copyright � 1998 Equivalence Pty. Ltd."; Pos  5, 80;  Dim 150, 9; }
    CentreText { Title "support@equival.com.au";      Pos  5, 90;  Dim 150, 9; }

    StaticBox  {                                      Pos  5, 100; Dim 150, 1; }

    PushButton { Title "Ok";  Options ok, default;    Pos 55, 110; Dim 45, 15; }
}


ModalDialog {
    Title "Parse URL";
    Class ParseURLDialog;

    RightText  { Pos 10,15;  Dim 29,8;   Title "URL:"; }
    ComboBox   { Pos 40,13;  Dim 200,90; Value url;
        StringList {
            "http://website.com/path/to/here",
            "http://fred@website.com/path/to/here",
            "http://fred:secret@website.com:123/path/to/here",
            "/a/uri/path",
            "file://localhost/root/path/to/there",
            "file:///root/path/to/there",
            "file:relative/path/to/there",
            "ftp://annoymous.com/path/to/it",
            "ftp://fred@authenticated.com/path/to/it",
            "ftp://fred:secret@authenticated.com:123/path/to/it",
            "mailto:fred@nurk.com",
            "mailto:fred@nurk.com?subject=Hi+there",
            "h323:fred@nurk.com",
            "h323:fred@nurk.com;faststart=false",
            "sip:fred@nurk.com",
            "sip:fred:secret@nurk.com:5070;transport=tcp"
        }
    }

    CheckBox   { Pos 50, 30; Dim 50,12;  Title "File Path"; Value filePath; }

    PushButton { Pos 40, 55; Dim 45, 15; Title "Ok";	 Options ok, default; }
    PushButton { Pos 130,55; Dim 45, 15; Title "Cancel"; Options cancel;      }
}


///////////////////////////////////////////////////////////////////////////////
