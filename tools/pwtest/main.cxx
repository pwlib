/*
 * main.cxx
 *
 * PWLib application source file for pwtest
 *
 * Main program entry point.
 *
 * Copyright 98 Equivalence
 *
 * $Log$
 * Revision 1.41  2004/04/03 06:54:33  rjongbloed
 * Many and various changes to support new Visual C++ 2003
 *
 * Revision 1.40  2003/12/14 10:00:04  rjongbloed
 * Updated to new API due to plug-ins
 *
 * Revision 1.39  2002/12/10 04:47:18  robertj
 * Added support in PTime for ISO 8601 format.
 *
 * Revision 1.38  2002/12/04 00:19:04  robertj
 * Removed get document that just returns a content length as the chunked
 *   transfer encoding makes this very dangerous.
 *
 * Revision 1.37  2002/12/02 00:16:13  robertj
 * Additional URL testing
 *
 * Revision 1.36  2002/11/19 10:37:32  robertj
 * Added functions to set anf get "file:" URL. as PFilePath and do the right
 *   things with platform dependent directory components.
 *
 * Revision 1.35  2002/05/21 04:23:40  robertj
 * Fixed problem with ASN encoding/decoding unsconstrained negative numbers,
 *
 * Revision 1.34  2002/03/18 05:03:03  robertj
 * Fixed display of URL test so does not treat & as special in dialog.
 *
 * Revision 1.33  2001/12/06 04:06:03  robertj
 * Removed "Win32 SSL xxx" build configurations in favour of system
 *   environment variables to select optional libraries.
 *
 * Revision 1.32  2001/11/08 00:31:27  robertj
 * Reinstated URL test code and added "parameter" string dictionary display.
 *
 * Revision 1.31  2001/09/28 10:05:57  robertj
 * Added check for "scientific" mode in PTimeInterval so does not print
 *   out hours and minutes, but just rounded seconds.
 *
 * Revision 1.30  2001/05/10 02:43:23  robertj
 * Added SSL test code.
 *
 * Revision 1.29  2001/03/20 06:56:07  robertj
 * Removed hard coded personal e-mail addreses from test.
 *
 * Revision 1.28  2001/03/05 04:18:27  robertj
 * Added net mask to interface info returned by GetInterfaceTable()
 *
 * Revision 1.27  2001/03/03 05:06:31  robertj
 * Major upgrade of video conversion and grabbing classes.
 *
 * Revision 1.26  2001/01/04 03:39:14  craigs
 * Removed IP addresses that cause DNS reverse lookup hassles
 *
 * Revision 1.25  2000/11/12 23:32:21  craigs
 * Added conditionals for functions not implemented in Motif/Qt libraries
 *
 * Revision 1.24  2000/11/10 01:08:11  robertj
 * Added content transfer encoding and automatic base64 translation.
 *
 * Revision 1.23  2000/11/09 05:50:23  robertj
 * Added RFC822 aware channel class for doing internet mail.
 *
 * Revision 1.22  2000/11/09 00:23:05  robertj
 * Additional video testing.
 *
 * Revision 1.21  2000/08/30 03:17:00  robertj
 * Improved multithreaded reliability of the timers under stress.
 *
 * Revision 1.20  2000/07/25 13:14:07  robertj
 * Got the video capture stuff going!
 *
 * Revision 1.19  2000/07/15 09:47:35  robertj
 * Added video I/O device classes.
 *
 * Revision 1.18  2000/04/29 08:11:06  robertj
 * Fixed problem with stream output width in PTimeInterval
 *
 * Revision 1.17  2000/04/29 04:51:07  robertj
 * Added microseconds to string output.
 * Fixed uninitialised microseconds on some constructors.
 *
 * Revision 1.16  1999/09/10 04:35:43  robertj
 * Added Windows version of PIPSocket::GetInterfaceTable() function.
 *
 * Revision 1.15  1999/08/07 07:11:14  robertj
 * Added test for menu text change
 *
 * Revision 1.14  1999/08/07 01:47:18  robertj
 * Fixed some trivial errors.
 *
 * Revision 1.13  1999/07/06 04:46:01  robertj
 * Fixed being able to case an unsigned to a PTimeInterval.
 * Improved resolution of PTimer::Tick() to be millisecond accurate.
 *
 * Revision 1.12  1999/07/03 03:12:59  robertj
 * #ifdef'ed test code for stuff not in general distrubution.
 *
 * Revision 1.11  1999/06/30 08:57:20  robertj
 * Fixed bug in encodeing sequence of constrained primitive type. Constraint not set.
 * Fixed bug in not emitting namespace use clause.
 * Added "normalisation" of separate sequence of <base type> to be single class.
 *
 * Revision 1.10  1999/06/24 14:00:09  robertj
 * Added URL parse test
 *
 * Revision 1.9  1999/06/14 07:59:39  robertj
 * Enhanced tracing again to add options to trace output (timestamps etc).
 *
 * Revision 1.8  1999/06/07 01:55:04  robertj
 * Added test for default driver function in sound classes.
 *
 * Revision 1.7  1999/05/28 14:07:18  robertj
 * Added function to get default audio device.
 *
 * Revision 1.6  1999/03/29 03:39:56  robertj
 * Changed semantics of PTitledWindow::OnClose() function.
 *
 * Revision 1.5  1999/02/23 07:11:29  robertj
 * Improved trace facility adding trace levels and #define to remove all trace code.
 *
 * Revision 1.4  1999/02/22 10:15:16  robertj
 * Sound driver interface implementation to Linux OSS specification.
 *
 * Revision 1.3  1999/01/31 00:59:27  robertj
 * Added IP Access Control List class to PTLib Components
 *
 * Revision 1.2  1998/12/23 01:17:29  robertj
 * PWCLib test
 *
 */

#include <pwlib.h>
#include <ptlib/videoio.h>
#include <ptclib/ipacl.h>
#include <ptclib/url.h>
#include <ptclib/asner.h>
#include <ptclib/random.h>
#include <ptclib/inetmail.h>
#if P_SSL
#include <ptclib/pssl.h>
#include <ptclib/http.h>
#endif

#include "resources.h"


class OpenSoundFileDialog : public POpenFileDialog
{
  PCLASSINFO(OpenSoundFileDialog, POpenFileDialog)
  public:
    OpenSoundFileDialog(MainWindow * parent);

    virtual void OnInit();

    const PString & GetDriver() const { return driver; }
    BOOL GetWait() const { return waitFlag; }

  private:
    PString driver;
    BOOL    waitFlag;
};


class SaveSoundFileDialog : public PSaveFileDialog
{
  PCLASSINFO(SaveSoundFileDialog, PSaveFileDialog)
  public:
    SaveSoundFileDialog(MainWindow * parent);

    virtual void OnInit();

    const PString & GetDriver() const { return driver; }

  private:
    PString driver;
};


PCREATE_PROCESS(Pwtest);

#define new PNEW


Pwtest::Pwtest()
  : PApplication("Equivalence", "pwtest", 1, 0, AlphaCode, 1)
{
  PTrace::SetLevel(1);
  PTrace::SetOptions(PTrace::Blocks);
}


#define TEST_TIME(t) PTRACE(1, t << " => " << PTime(t))

void Pwtest::Main()
{
  PTRACE_BLOCK("Pwtest::Pwtest()");
  PTRACE(2, GetName() << " v" << GetVersion(TRUE));

  PTime now;
  PTRACE(1, "Time is now " << now.AsString("h:m:s.u d/M/y"));
  PTRACE(1, "Time is now " << now.AsString("yyyy/MM/dd h:m:s.uuuu"));
  PTRACE(1, "Time is now " << now.AsString("MMM/d/yyyyy w h:m:sa"));
  PTRACE(1, "Time is now " << now.AsString("wwww d/M/yyy h:m:s.uu"));
  PTRACE(1, "Time is now " << now.AsString("www d/MMMM/yy h:m:s.uuu"));

  TEST_TIME("20010203T1234Z");
  TEST_TIME("20010203T1234");
  TEST_TIME("20010203T0034");
  TEST_TIME("20010203T10034");
  TEST_TIME("20010203T123456+1100");
  TEST_TIME("20010203T000056");
  TEST_TIME("20010203T123456");
  TEST_TIME("2001-02-03 T 12:34:56");
  TEST_TIME("5/03/1999 12:34:56");
  TEST_TIME("15/06/1999 12:34:56");
  TEST_TIME("15/06/01 12:34:56 PST");
  TEST_TIME("5/06/02 12:34:56");
  TEST_TIME("5/23/1999 12:34am");
  TEST_TIME("5/23/00 12:34am");
  TEST_TIME("1999/23/04 12:34:56");
  TEST_TIME("Mar 3, 1999 12:34pm");
  TEST_TIME("3 Jul 2004 12:34pm");
  TEST_TIME("12:34:56 5 December 1999");
  TEST_TIME("10 minutes ago");
  TEST_TIME("2 weeks");

  PTRACE(1, "Testing timer resolution, reported as " << PTimer::Resolution() << "ms");
  time_t oldSec = time(NULL);   // Wait for second boundary
  while (oldSec == time(NULL))
    ;

  oldSec++;
  PTimeInterval newTick = PTimer::Tick();
  PTimeInterval oldTick = newTick;
  unsigned count = 0;

  while (oldSec == time(NULL)) {  // For one full second
    while (newTick == oldTick)
      newTick = PTimer::Tick();
    oldTick = newTick;
    count++;                      // Count the changes in tick
  } ;

  PTRACE(1, "Actual resolution is " << 1000000/count << "us");

  oldTick = 123456;
  PTRACE(1, "TimeInterval output: \"" << setw(15) << newTick << '"');
  PTRACE(1, "TimeInterval output: \"" << setw(15) << oldTick << '"');
  int p;
  for (p = 3; p < 10; p++)
    PTRACE(1, "TimeInterval output: " << p << " \""
           << setiosflags(ios::scientific)
           << setw(p) << setprecision(2) << oldTick
           << resetiosflags(ios::scientific) << '"');
  for (p = 3; p < 20; p++)
    PTRACE(1, "TimeInterval output: " << p << " \""
           << setw(p) << setprecision(2) << oldTick << '"');

  PPER_Stream strm;
  PASN_ObjectId id;
  id = "0.0.8.2250.0.2";
  id.Encode(strm);
  strm.CompleteEncoding();
  PTRACE(1, "OID " << id << " -> " << strm);

  PASN_Integer asn_int1, asn_int2;
  asn_int1 = 123456789;
  strm.BeginEncoding();
  asn_int1.Encode(strm);
  strm.CompleteEncoding();
  strm.ResetDecoder();
  asn_int2.Decode(strm);
  PTRACE(1, "ASN +ve Integer " << asn_int1 << " -> " << strm << " -> " << asn_int2);

  asn_int1 = (unsigned)-12345;
  strm.BeginEncoding();
  asn_int1.Encode(strm);
  strm.CompleteEncoding();
  strm.ResetDecoder();
  asn_int2.Decode(strm);
  PTRACE(1, "ASN -ve Integer " << asn_int1 << " -> " << strm << " -> " << asn_int2);

  SetAboutDialogID(IDD_ABOUT);

  new MainWindow(GetArguments());

  PApplication::Main();
}


MainWindow::MainWindow(PArgList &)
{
  PTRACE_BLOCK("Pwtest::Pwtest()");

  SetTitle(PResourceString(IDS_TITLE));
  SetIcon(PIcon(IDI_MAIN_WINDOW));
  myMenu = new MainMenu(this);
  SetMenu(myMenu);

#if ! defined(P_MOTIF) && ! defined(P_LESSTIF) && ! defined(P_QT)
  static const PButtonBar::ButtonID cmds[] = {
    { 1, "IP_ACL", IDM_TEST_ENABLED, IDM_TEST_DISABLED, IDS_TEST_BUTTON }
  };
  buttonBar = new PButtonBar(this, cmds, PARRAYSIZE(cmds));
#endif

  statusBar = new PStatusBar(this, 2);
  statusBar->SetSectionWidth(1, 20);

#if ! defined(P_MOTIF) && ! defined(P_LESSTIF) && ! defined(P_QT)
  new PMDIDocWindow(this, "Test");
#endif

  UpdateCommandSources();
  ShowAll();
}


void MainWindow::NewCmd(PMenuItem &, INT)
{
#if ! defined(P_MOTIF) && ! defined(P_LESSTIF) && ! defined(P_QT)
  PMDIDocWindow * child = new PMDIDocWindow(this, "fred");
  child->Show();
#endif
}


void MainWindow::OpenCmd(PMenuItem &, INT)
{
  POpenFileDialog dlg(this);
  if (dlg.RunModal()) {
    PTextFile file;
    if (file.Open(dlg.GetFile())) {
      PString str = file.ReadString(file.GetLength());
    }
  }
}


void MainWindow::CloseCmd(PMenuItem &, INT)
{
  // Close document
}


void MainWindow::SaveCmd(PMenuItem & item, INT)
{
  // Save current document
  PINDEX pos;
  pos = item.GetPosition();
  pos = item.GetMenu()->GetPosition();
}


void MainWindow::SaveAsCmd(PMenuItem &, INT)
{
  PSaveFileDialog dlg(this);
  if (dlg.RunModal()) {
    // Save document to new name
  }
}


void MainWindow::PrintCmd(PMenuItem &, INT)
{
  PPrintJobDialog dlg(this, printInfo);
  if (dlg.RunModal()) {
    printInfo = dlg.GetPrintInfo();
    PPrintCanvas canvas("pwtest", printInfo);

    // Add printing code here
  }
}


void MainWindow::PrinterSetupCmd(PMenuItem &, INT)
{
  PPrinterSetupDialog dlg(this, printInfo);
  if (dlg.RunModal())
    printInfo = dlg.GetPrintInfo();
}


void MainWindow::ExitCmd(PMenuItem &, INT)
  // The Exit menu ... well ... exits.
{
  owner->Terminate();
}


void MainWindow::CopyCmd()
{
  PClipboard clip(this);
  // Do something with the clipboard
}


BOOL MainWindow::CanCopy()
{
  // If want copy menu enabled
  return TRUE;
}


void MainWindow::PasteCmd()
{
  PClipboard clip(this);
  // Do something with the clipboard
}


BOOL MainWindow::CanPaste()
{
  // If want paste menu enabled, ie clipboard has right format
  return TRUE;
}


void MainWindow::OnRedraw(PCanvas & canvas)
{
  PPattern::Bits pat = { 0x81, 0x42, 0x24, 0x18, 0x18, 0x24, 0x42, 0x81 };
  canvas.SetFillPattern(PPattern(pat));
  canvas.FillRect(100, 100, 100, 100);
  canvas.FillRect(150, 150, 100, 100);
}


void MainWindow::OnResize(const PDim & newSize, ResizeType type)
{
  if (type != Iconic) {
    PRect bounds(newSize);
#if ! defined(P_MOTIF) && ! defined(P_LESSTIF) && ! defined(P_QT)
    buttonBar->AutoAdjustBounds(bounds, AdjustTop);
#endif
    statusBar->AutoAdjustBounds(bounds, AdjustBottom);
#if ! defined(P_MOTIF) && ! defined(P_LESSTIF) && ! defined(P_QT)
    SetDocumentArea(bounds, PixelCoords);
#endif
  }
}


void MainWindow::OnClose()
{
  PTopLevelWindow::OnClose();
}



void MainWindow::TestCmd()
{
  myMenu->menuTestItem->SetString("Test Menu");
}


void MainWindow::IP_ACL_Cmd()
{
  PIpAccessControlList list;

  list.LoadHostsAccess("ipop3d");
  list.Add("10.0.2.0/24");
  list.Add("192.168.");
  list.Add(".nr.equival.net");
  list.Add("-hadron.nr.equival.net");
  list.Add("-10.0.2.1");
  list.Add("203.41.8.86/255.255.255.248");
  list.Add("-ALL");

  PINDEX i;
  PStringStream str;
  for (i = 0; i < list.GetSize(); i++)
    str << list[i] << '\n';

  static const char * const addresses[] = {
    "10.0.1.1", "10.0.1.4", "10.0.2.4", "10.0.2.1",
    //"129.168.1.1", "192.168.1.1",
    //"203.41.8.86", "203.41.8.80", "203.41.8.88"
  };
  for (i = 0; i < PARRAYSIZE(addresses); i++) {
    PIPSocket::Address addr(addresses[i]);
    str << '\n' << addr << ' ' << (list.IsAllowed(addr) ? "Allowed" : "Disallowed");
  }

  PSimpleDialog::Info(this, str);
}


void MainWindow::ListInterfacesCmd(PMenuItem &, INT)
{
  PStringStream str;

  PIPSocket::InterfaceTable interfaces;
  if (PIPSocket::GetInterfaceTable(interfaces)) {
    str << "Interfaces:\n";
    for (PINDEX i = 0; i < interfaces.GetSize(); i++)
      str << "    "
          << interfaces[i].GetName() << ' '
          << interfaces[i].GetAddress() << ' '
          << interfaces[i].GetNetMask() << ' '
          << interfaces[i].GetMACAddress() << '\n';
  }
  else
    str << "Error getting interfaces.";

  PSimpleDialog::Info(this, str);
}


void MainWindow::ParseURLCmd()
{
  ParseURLDialog dlg(this);

  dlg.filePath = FALSE;

  if (!dlg.RunModal())
    return;

  PURL url;

  if (dlg.filePath) {
    PFilePath path = dlg.url;
    url = path;
  }
  else
    url = dlg.url;

  PStringStream str;

  str << "URL: " << url << "\n"
         "Scheme: " << url.GetScheme() << "\n"
         "Host: " << url.GetHostName() << "\n"
         "Port: " << url.GetPort() << "\n"
         "Username: " << url.GetUserName() << "\n"
         "Password: " << url.GetPassword() << "\n"
         "Fragment: " << url.GetFragment() << "\n";

  PINDEX i;
  if (url.GetPath().IsEmpty())
    str << "Path: " << url.GetPathStr() << '\n';
  else {
    str << (url.GetRelativePath() ? "Relative" : "Absolute") << '\n';
    for (i = 0; i < url.GetPath().GetSize(); i++)
      str << "Path[" << i << "]: " << url.GetPath()[i] << '\n';
  }

  for (i = 0; i < url.GetParamVars().GetSize(); i++)
    str << "Param[" << i << "]: "
        << url.GetParamVars().GetKeyAt(i) << " = "
        << url.GetParamVars().GetDataAt(i) << '\n';

  for (i = 0; i < url.GetQueryVars().GetSize(); i++)
    str << "Query[" << i << "]: "
        << url.GetQueryVars().GetKeyAt(i) << " = "
        << url.GetQueryVars().GetDataAt(i) << '\n';

  str << "FilePath: " << url.AsFilePath() << '\n';

  str.Replace("&", "&&", TRUE);

  PSimpleDialog::Info(this, str);
}


void MainWindow::PlaySoundCmd()
{
  OpenSoundFileDialog dlg(this);
  if (!dlg.RunModal())
    return;

  PSoundChannel player;
  if (!player.Open(dlg.GetDriver(), PSoundChannel::Player)) {
    PSimpleDialog::Error(this, "Could not open driver: "+dlg.GetDriver());
    return;
  }

  player.SetBuffers(65536, 8);

  if (!player.PlayFile(dlg.GetFile(), dlg.GetWait()))
    PSimpleDialog::Error(this, "Could not play file: "+dlg.GetFile());
}


OpenSoundFileDialog::OpenSoundFileDialog(MainWindow * parent)
  : POpenFileDialog(parent)
{
  waitFlag = TRUE;
  AddFileType(".wav");
}


void OpenSoundFileDialog::OnInit()
{
  POpenFileDialog::OnInit();

  PDim dim = GetDimensions(LocalCoords);

  driver = PSoundChannel::GetDefaultDevice(PSoundChannel::Player);
  PStringArray driverNames = PSoundChannel::GetDeviceNames(PSoundChannel::Player);

  PComboBox * drivers = new PComboBox(this);
  drivers->SetValuePointer(&driver);
  for (PINDEX i = 0; i < driverNames.GetSize(); i++) {
    drivers->AddString(driverNames[i]);
    if (driverNames[i] == driver)
      drivers->SetCurrent(i);
  }
  drivers->SetPosition(20, dim.Height());
  drivers->SetDimensions(120, 60, LocalCoords);
  drivers->Show();

  dim.AddHeight(20);
  if (dim.Width() <= 160)
    dim.SetWidth(160);

  PCheckBox * wait = new PCheckBox(this, "Wait", waitFlag);
  wait->SetValuePointer(&waitFlag);
  wait->SetPosition(20, dim.Height());
  wait->Show();

  PDim ctlDim = wait->GetDimensions(LocalCoords);
  dim.AddHeight(ctlDim.Height() + 10);
  if (dim.Width() <= ctlDim.Width()+40)
    dim.SetWidth(ctlDim.Width()+41);

  SetDimensions(dim, LocalCoords);
}


void MainWindow::RecordSoundCmd()
{
  SaveSoundFileDialog dlg(this);
  if (!dlg.RunModal())
    return;

  PSoundChannel recorder;
  if (!recorder.Open(dlg.GetDriver(), PSoundChannel::Recorder)) {
    PSimpleDialog::Error(this, "Could not open driver: "+dlg.GetDriver());
    return;
  }

  recorder.SetBuffers(8*8000, 2); // 8 x 8 second buffers

  if (!recorder.RecordFile(dlg.GetFile()))
    PSimpleDialog::Error(this, "Could not record file: "+dlg.GetFile());
}


SaveSoundFileDialog::SaveSoundFileDialog(MainWindow * parent)
  : PSaveFileDialog(parent)
{
  SetDefaultFileType(".wav");
}


void SaveSoundFileDialog::OnInit()
{
  PSaveFileDialog::OnInit();

  PDim dim = GetDimensions(LocalCoords);

  driver = PSoundChannel::GetDefaultDevice(PSoundChannel::Recorder);
  PStringArray driverNames = PSoundChannel::GetDeviceNames(PSoundChannel::Recorder);

  PComboBox * drivers = new PComboBox(this);
  drivers->SetValuePointer(&driver);
  for (PINDEX i = 0; i < driverNames.GetSize(); i++) {
    drivers->AddString(driverNames[i]);
    if (driverNames[i] == driver)
      drivers->SetCurrent(i);
  }
  drivers->SetPosition(20, dim.Height());
  drivers->SetDimensions(120, 60, LocalCoords);
  drivers->Show();

  dim.AddHeight(20);
  if (dim.Width() <= 160)
    dim.SetWidth(160);

  SetDimensions(dim, LocalCoords);
}


void MainWindow::VideoCaptureCmd()
{
  PStringList drivers = PVideoInputDevice::GetDriverNames();
  if (drivers.IsEmpty()) {
    PSimpleDialog::Error(this, "No video grabber drivers.");
    return;
  }

  PVideoInputDevice * grabber = PVideoInputDevice::CreateDevice(drivers[0]);
  if (grabber == NULL) {
    PSimpleDialog::Error(this, "Cannot create video input device for driver " + drivers[0]);
    return;
  }

  PStringList devices = grabber->GetDeviceNames();
  if (devices.IsEmpty()) {
    PSimpleDialog::Error(this, "No video grabber devices.");
    return;
  }

  if (!grabber->Open(devices[0], FALSE)) {
    PSimpleDialog::Error(this, "Could not open device " + devices[0]);
    return;
  }

  grabber->SetFrameRate(25);
  grabber->SetColourFormatConverter("RGB32");

  grabber->Start();

  PTimeInterval startTime = PTimer::Tick();
  PBYTEArray buffer(grabber->GetMaxFrameBytes());
  PINDEX i;
  for (i = 0; i < 100; i++) {
    if (!grabber->GetFrameData(buffer.GetPointer()))
      break;
  }

  grabber->Stop();

  if (i < 100)
    PSimpleDialog::Error(this, "Error in grab.");
  else {
    long time = (long)(PTimer::Tick() - startTime).GetMilliSeconds();
    PSimpleDialog::Info(this, "Grabbed 100 frames in %lu ms (%u f/s).", time, 100000/time);

    unsigned width, height;
    grabber->GetFrameSize(width, height);
    PPixelImage image(width, height, 32);
    memcpy(image->GetPixelDataPtr(), buffer, buffer.GetSize());
    PFile file("c:\\capture.bmp", PFile::WriteOnly);
    image->Write(file);
  }

  delete grabber;
}


void MainWindow::StressTimeout(PTimer & timer, INT)
{
  TimerStressReport << PTimer::Tick().GetMilliSeconds() << "\tTimeout\t" << (void *)&timer << endl;
  timer = PRandom::Number()%10000;
}


void MainWindow::TimerStressCmd()
{
  if (!TimerStressReport.Open("TimerStress.txt", PFile::WriteOnly)) {
    PSimpleDialog::Error(this, "Could not open \"TimerStress.txt\"!");
    return;
  }

  PINDEX i;
  PTimer * timers[1000];

  for (i = 0; i < PARRAYSIZE(timers); i++)
    timers[i] = NULL;

  static const PINDEX numTests = 100000;
  PINDEX activeTimers = 0;
  PINDEX highWatermark = 0;

  for (i = 0; i < numTests; i++) {
    PINDEX t = PRandom::Number()%PARRAYSIZE(timers);
    if (timers[t] != NULL) {
      TimerStressReport << PTimer::Tick().GetMilliSeconds() << "\tDestroy\t" << (void *)timers[t] << endl;
      delete timers[t];
      timers[t] = NULL;
      activeTimers--;
    }
    else {
      timers[t] = new PTimer(PRandom::Number()%10000);
      timers[t]->SetNotifier(PCREATE_NOTIFIER(StressTimeout));
      TimerStressReport << PTimer::Tick().GetMilliSeconds() << "\tCreate\t" << (void *)timers[t] << endl;
      activeTimers++;
      if (activeTimers > highWatermark)
        highWatermark = activeTimers;
    }
    PThread::Current()->Sleep(PRandom::Number()%100);

    if ((i%(numTests/100)) == 0) {
      PTRACE(1, "Timer stress tested " << (i*100/numTests) << '%');
    }
  }

  PTRACE(1, "Timer stress tested 100% (" << numTests
         << " iterations), max simultaneous timers: " << highWatermark);

  for (i = 0; i < PARRAYSIZE(timers); i++)
    delete timers[i];

  TimerStressReport.Close();
}


void MainWindow::MailTestCmd()
{
  {
    PRFC822Channel simple(PRFC822Channel::Sending);
    simple.SetFromAddress("postmaster");
    simple.SetToAddress(PProcess::Current().GetUserName());
    simple.SetSubject("Test of RFC822 class.");

    if (!simple.SendWithSMTP("mail"))
      PSimpleDialog::Error(this, "Error in sending message.");

    simple << "This is a simple test.\n\n";
    for (PINDEX i = 1; i < 10; i++)
      simple << "  Line number " << i << "\n";
    simple << "\nEnd of test.\n";
  }

  PRFC822Channel complex(PRFC822Channel::Sending);
  complex.SetFromAddress("postmaster");
  complex.SetToAddress(PProcess::Current().GetUserName());
  complex.SetSubject("Test of RFC822 class.");

  if (!complex.SendWithSMTP("mail"))
    PSimpleDialog::Error(this, "Error in sending message.");

  PString boundary = complex.MultipartMessage();

  complex << "\nThis is the first part of a multipart message.\n";
  complex.NextPart(boundary);

  complex << "\n\nThis is the second part of a multipart message.\n";
  complex.NextPart(boundary);

  complex.SetContentType("text/html");
  complex << "<HTML><BODY>\n"
             "<H1>Third Page</H1>\n"
             "This is the third part of a multipart message.\n"
             "</BODY></HTML>";

  complex.NextPart(boundary);
  complex.SetContentAttachment("plushot.gif");
  complex.SetTransferEncoding("base64");
  static BYTE binaryStuff[] = {
    0x47, 0x49, 0x46, 0x38, 0x39, 0x61, 0x09, 0x00,
    0x09, 0x00, 0xA2, 0x00, 0x00, 0x00, 0x00, 0xFF,
    0x5F, 0x8A, 0xC5, 0xFF, 0xFF, 0xFF, 0xCC, 0xCC,
    0xCC, 0x66, 0x66, 0x66, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x2C, 0x00, 0x00,
    0x00, 0x00, 0x09, 0x00, 0x09, 0x00, 0x00, 0x03,
    0x1D, 0x4A, 0xBA, 0x5A, 0x24, 0x30, 0x0A, 0x27,
    0xC6, 0x00, 0x76, 0xD0, 0x11, 0x40, 0xB0, 0x14,
    0x20, 0x8A, 0xDA, 0xC3, 0x79, 0xA0, 0x79, 0x65,
    0x5B, 0x06, 0x16, 0x70, 0x1C, 0x27, 0x00, 0x3B
  };
  complex.Write(binaryStuff, sizeof(binaryStuff));

  complex.NextPart(boundary);
  complex.SetTransferEncoding("8bit");
  complex.SetContentType("text/plain");
  complex << "This is the last part of a multipart message.\n";
}


#if P_SSL
void MainWindow::TestSSLCmd()
{
  PTCPSocket tcp(443);
  if (tcp.Connect("www.microtelco.com")) {
    PSSLChannel ssl;
    if (ssl.Connect(tcp)) {
      PHTTPClient http;
      if (http.Open(ssl)) {
        PString content;
        if (http.GetTextDocument("/", content))
          PSimpleDialog::Info(this, "Success!\n" + content.Left(200));
        else
          PSimpleDialog::Error(this, "HTTP GET from server failed: " + http.GetErrorText());
      }
      else
        PSimpleDialog::Error(this, "HTTP connect to server failed: " + http.GetErrorText());
    }
    else
      PSimpleDialog::Error(this, "SSL connect to server failed: " + ssl.GetErrorText());
  }
  else
    PSimpleDialog::Error(this, "TCP connect to server failed: " + tcp.GetErrorText());
}
#endif

// End of File ///////////////////////////////////////////////////////////////
